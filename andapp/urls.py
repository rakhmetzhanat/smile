"""andapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from webapp.views import comment, message_user, message_customer, friend_delete, friend_add, busket, busket_delete, dream, dream_delete, like, logout, check_user, insert_user, index_main_content, index_customer_main, index_main, index_registration, index_search, index_profile, index_busket, index_dream, index_store, index_about, customer_main, customer_profile, customer_goods, customer_messages, customer_statistics, customer_main_content, customer_main_content_edit, insert_customer, check_customer, insert_product, update_product

urlpatterns = [
    url(r'^admin/$', include(admin.site.urls)),
    url(r'^$', index_main),
    url(r'^registration/$', index_registration),
    url(r'^search/$', index_search),
    url(r'^logout/$', logout),

    url(r'^index_main_content/$', index_main_content),
    url(r'^index_customer_main/$', index_customer_main),
    url(r'^profile/$', index_profile),
    url(r'^basket/$', index_busket),   
    url(r'^dream/$', index_dream),
    url(r'^store/$', index_store), 
    url(r'^about/$', index_about),     
    url(r'^insert_user/$', insert_user),
    url(r'^check_user/$', check_user),
    url(r'^like/$', like),
    url(r'^dream_insert/$', dream),
    url(r'^dream_delete/$', dream_delete),
    url(r'^busket/$', busket), 
    url(r'^busket_delete/$', busket_delete),
    url(r'^friend_add/$', friend_add),
    url(r'^friend_delete/$', friend_delete), 
    url(r'^message_user/$', message_user),
    url(r'^message_customer/$', message_customer),
    url(r'^comment/$', comment), 
    
        

    url(r'^customer_main/$', customer_main),
    url(r'^customer_profile/$', customer_profile),
    url(r'^customer_goods/$', customer_goods),
    url(r'^customer_messages/$', customer_messages),
    url(r'^customer_statistics/$', customer_statistics),
    
    url(r'^insert_customer/$', insert_customer),
    url(r'^check_customer/$', check_customer),
    url(r'^update_product/$', update_product),
    url(r'^insert_product/$', insert_product),

    url(r'^customer_main_content/$', customer_main_content),
    url(r'^customer_main_content_edit/$', customer_main_content_edit),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
