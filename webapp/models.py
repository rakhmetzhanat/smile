from django.db import models
from django.utils import timezone

class Users(models.Model):
	user = models.CharField(max_length=50, default="user")
	surname = models.CharField(max_length=50)
	name = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	telephone = models.CharField(max_length=50)
	image = models.ImageField(null=True, blank=True, default = 'users.png', upload_to='users/')
	password = models.CharField(max_length=50)	
	busket_price = models.IntegerField(blank=True, default=0)
	busket_num = models.IntegerField(blank=True, default=0)
	date_created = models.DateTimeField(default=timezone.now)




class Customer(models.Model):
	surname = models.CharField(max_length=50)
	name = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	telephone = models.CharField(max_length=50)
	image = models.ImageField(null=True, blank=True, default = 'customer.png', upload_to='customer/')
	city = models.CharField(max_length=50)
	password = models.CharField(max_length=50)
	products_sum = models.IntegerField(blank=True, default=0)
	dream_sum = models.IntegerField(blank=True, default=0)
	comment_sum = models.IntegerField(blank=True, default=0)
	date_created = models.DateTimeField(default=timezone.now)




class Friend(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	customer = models.ForeignKey('Customer',null=True, default = None)
	date_created = models.DateTimeField(default=timezone.now)




class Message(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	customer = models.ForeignKey('Customer',null=True, default = None)
	content = models.CharField(max_length=500)
	sender =  models.CharField(max_length=50)
	date_created = models.DateTimeField(default=timezone.now)




class Products(models.Model):
	customer = models.ForeignKey('Customer',null=True, default = None)

	image_1 = models.ImageField(null=True, blank=True, default = 'products.svg', upload_to='products/')
	image_2 = models.ImageField(null=True, blank=True, default = 'products.svg', upload_to='products/')
	image_3 = models.ImageField(null=True, blank=True, default = 'products.svg', upload_to='products/')
	image_4 = models.ImageField(null=True, blank=True, default = 'products.svg', upload_to='products/')

	price = models.IntegerField(default=0)
	title = models.CharField(max_length=500)
	content = models.CharField(max_length=5000)
	dream_num = models.IntegerField(blank=True, default=0)
	sales_num = models.IntegerField(blank=True, default=0)
	like_num = models.IntegerField(blank=True, default=0)
	date_created = models.DateTimeField(default=timezone.now)




class Comments(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	product = models.ForeignKey('Products',null=True, default = None)
	comment = models.CharField(max_length=500)
	date_created = models.DateTimeField(default=timezone.now)




class Dream(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	product = models.ForeignKey('Products',null=True, default = None)
	date_created = models.DateTimeField(default=timezone.now)




class Like(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	product = models.ForeignKey('Products',null=True, default = None)
	date_created = models.DateTimeField(default=timezone.now)




class Busket(models.Model):
	user = models.ForeignKey('Users',null=True, default = None)
	product = models.ForeignKey('Products',null=True, default = None)
	date_created = models.DateTimeField(default=timezone.now)