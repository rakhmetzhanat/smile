# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0010_products_like_num'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='user',
            field=models.CharField(default=b'user', max_length=50),
        ),
    ]
