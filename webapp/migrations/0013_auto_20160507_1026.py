# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0012_auto_20160507_1011'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='comments',
        ),
        migrations.RemoveField(
            model_name='products',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='users',
            name='busket',
        ),
        migrations.RemoveField(
            model_name='users',
            name='dream',
        ),
        migrations.DeleteModel(
            name='Busket',
        ),
        migrations.DeleteModel(
            name='Comments',
        ),
        migrations.DeleteModel(
            name='Customer',
        ),
        migrations.DeleteModel(
            name='Dream',
        ),
        migrations.DeleteModel(
            name='Products',
        ),
        migrations.DeleteModel(
            name='Users',
        ),
    ]
