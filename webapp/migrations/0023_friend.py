# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0022_auto_20160508_1641'),
    ]

    operations = [
        migrations.CreateModel(
            name='Friend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('customer', models.ForeignKey(default=None, to='webapp.Customer', null=True)),
                ('user', models.ForeignKey(default=None, to='webapp.Users', null=True)),
            ],
        ),
    ]
