# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0029_auto_20160510_1611'),
    ]

    operations = [
        migrations.CreateModel(
            name='Busket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=500)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('surname', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('telephone', models.CharField(max_length=50)),
                ('image', models.ImageField(default=b'customer.png', null=True, upload_to=b'customer/', blank=True)),
                ('city', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('products_sum', models.IntegerField(default=0, blank=True)),
                ('dream_sum', models.IntegerField(default=0, blank=True)),
                ('comment_sum', models.IntegerField(default=0, blank=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Dream',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Friend',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('customer', models.ForeignKey(default=None, to='webapp.Customer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Like',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=500)),
                ('sender', models.CharField(max_length=50)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('customer', models.ForeignKey(default=None, to='webapp.Customer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_1', models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True)),
                ('image_2', models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True)),
                ('image_3', models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True)),
                ('image_4', models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True)),
                ('price', models.IntegerField(default=0)),
                ('title', models.CharField(max_length=50)),
                ('content', models.CharField(max_length=5000)),
                ('dream_num', models.IntegerField(default=0, blank=True)),
                ('sales_num', models.IntegerField(default=0, blank=True)),
                ('like_num', models.IntegerField(default=0, blank=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('customer', models.ForeignKey(default=None, to='webapp.Customer', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.CharField(default=b'user', max_length=50)),
                ('surname', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('telephone', models.CharField(max_length=50)),
                ('image', models.ImageField(default=b'users.png', null=True, upload_to=b'users/', blank=True)),
                ('password', models.CharField(max_length=50)),
                ('busket_price', models.IntegerField(default=0, blank=True)),
                ('busket_num', models.IntegerField(default=0, blank=True)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AddField(
            model_name='message',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='like',
            name='product',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AddField(
            model_name='like',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='friend',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='dream',
            name='product',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AddField(
            model_name='dream',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='comments',
            name='product',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AddField(
            model_name='comments',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='busket',
            name='product',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AddField(
            model_name='busket',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
    ]
