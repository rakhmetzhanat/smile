# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0004_delete_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='avatar_path',
        ),
        migrations.AddField(
            model_name='users',
            name='badge',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='users',
            name='borrows',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='users',
            name='incomes',
            field=models.CharField(default=1, max_length=50),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='users',
            name='email',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='users',
            name='name',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='users',
            name='password',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='users',
            name='surname',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='users',
            name='telephone',
            field=models.CharField(max_length=50),
        ),
    ]
