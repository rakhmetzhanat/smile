# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_users'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Category',
        ),
    ]
