# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0011_users_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='Busket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('users_id', models.CharField(max_length=50)),
                ('product_id', models.CharField(max_length=50)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('users_id', models.CharField(max_length=50)),
                ('product_id', models.CharField(max_length=50)),
                ('comment', models.CharField(max_length=500)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Dream',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('users_id', models.CharField(max_length=50)),
                ('product_id', models.CharField(max_length=50)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AddField(
            model_name='products',
            name='comments',
            field=models.ForeignKey(default=1, to='webapp.Comments'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='users',
            name='busket',
            field=models.ForeignKey(default=1, to='webapp.Busket'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='users',
            name='dream',
            field=models.ForeignKey(default=1, to='webapp.Dream'),
            preserve_default=False,
        ),
    ]
