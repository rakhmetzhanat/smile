# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0026_auto_20160510_0937'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='image_2',
            field=models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True),
        ),
        migrations.AddField(
            model_name='products',
            name='image_3',
            field=models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True),
        ),
        migrations.AddField(
            model_name='products',
            name='image_4',
            field=models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True),
        ),
        migrations.AlterField(
            model_name='customer',
            name='image',
            field=models.ImageField(default=b'customer.png', null=True, upload_to=b'customer/', blank=True),
        ),
        migrations.AlterField(
            model_name='products',
            name='image',
            field=models.ImageField(default=b'products.svg', null=True, upload_to=b'products/', blank=True),
        ),
        migrations.AlterField(
            model_name='users',
            name='image',
            field=models.ImageField(default=b'users.png', null=True, upload_to=b'users/', blank=True),
        ),
    ]
