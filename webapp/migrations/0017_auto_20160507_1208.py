# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0016_auto_20160507_1040'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='products',
            name='comments',
        ),
        migrations.RemoveField(
            model_name='users',
            name='busket',
        ),
        migrations.RemoveField(
            model_name='users',
            name='dream',
        ),
    ]
