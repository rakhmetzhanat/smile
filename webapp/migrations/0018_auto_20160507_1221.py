# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0017_auto_20160507_1208'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='busket',
            name='users_id',
        ),
        migrations.RemoveField(
            model_name='comments',
            name='users_id',
        ),
        migrations.RemoveField(
            model_name='dream',
            name='users_id',
        ),
        migrations.AddField(
            model_name='busket',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='comments',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AddField(
            model_name='dream',
            name='user',
            field=models.ForeignKey(default=None, to='webapp.Users', null=True),
        ),
        migrations.AlterField(
            model_name='busket',
            name='product_id',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AlterField(
            model_name='comments',
            name='product_id',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
        migrations.AlterField(
            model_name='dream',
            name='product_id',
            field=models.ForeignKey(default=None, to='webapp.Products', null=True),
        ),
    ]
