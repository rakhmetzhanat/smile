# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0015_auto_20160507_1037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='comments',
            field=models.ForeignKey(default=None, to='webapp.Comments', null=True),
        ),
        migrations.AlterField(
            model_name='products',
            name='customer',
            field=models.ForeignKey(default=None, to='webapp.Customer', null=True),
        ),
        migrations.AlterField(
            model_name='users',
            name='busket',
            field=models.ForeignKey(default=None, to='webapp.Busket', null=True),
        ),
        migrations.AlterField(
            model_name='users',
            name='dream',
            field=models.ForeignKey(default=None, to='webapp.Dream', null=True),
        ),
    ]
