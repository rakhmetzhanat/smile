# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0025_auto_20160510_0829'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='image',
            field=models.ImageField(default=b'avatar.png', null=True, upload_to=b'customer/', blank=True),
        ),
        migrations.AlterField(
            model_name='products',
            name='image',
            field=models.ImageField(default=b'avatar.png', null=True, upload_to=b'products/', blank=True),
        ),
        migrations.AlterField(
            model_name='users',
            name='image',
            field=models.ImageField(default=b'avatar.png', null=True, upload_to=b'users/', blank=True),
        ),
    ]
