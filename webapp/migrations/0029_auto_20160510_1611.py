# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0028_auto_20160510_1528'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='busket',
            name='product',
        ),
        migrations.RemoveField(
            model_name='busket',
            name='user',
        ),
        migrations.RemoveField(
            model_name='comments',
            name='product',
        ),
        migrations.RemoveField(
            model_name='comments',
            name='user',
        ),
        migrations.RemoveField(
            model_name='dream',
            name='product',
        ),
        migrations.RemoveField(
            model_name='dream',
            name='user',
        ),
        migrations.RemoveField(
            model_name='friend',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='friend',
            name='user',
        ),
        migrations.RemoveField(
            model_name='like',
            name='product',
        ),
        migrations.RemoveField(
            model_name='like',
            name='user',
        ),
        migrations.RemoveField(
            model_name='message',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='message',
            name='user',
        ),
        migrations.RemoveField(
            model_name='products',
            name='customer',
        ),
        migrations.DeleteModel(
            name='Busket',
        ),
        migrations.DeleteModel(
            name='Comments',
        ),
        migrations.DeleteModel(
            name='Customer',
        ),
        migrations.DeleteModel(
            name='Dream',
        ),
        migrations.DeleteModel(
            name='Friend',
        ),
        migrations.DeleteModel(
            name='Like',
        ),
        migrations.DeleteModel(
            name='Message',
        ),
        migrations.DeleteModel(
            name='Products',
        ),
        migrations.DeleteModel(
            name='Users',
        ),
    ]
