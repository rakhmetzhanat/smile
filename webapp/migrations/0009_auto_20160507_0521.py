# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0008_products_customer'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='badge',
        ),
        migrations.RemoveField(
            model_name='users',
            name='borrows',
        ),
        migrations.RemoveField(
            model_name='users',
            name='incomes',
        ),
    ]
