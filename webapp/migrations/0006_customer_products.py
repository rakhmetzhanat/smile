# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0005_auto_20160505_0911'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('surname', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=50)),
                ('telephone', models.CharField(max_length=50)),
                ('city', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=50)),
                ('products_sum', models.IntegerField(default=0)),
                ('dream_sum', models.IntegerField(default=0)),
                ('comment_sum', models.IntegerField(default=0)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=50)),
                ('content', models.CharField(max_length=5000)),
                ('rating_num', models.IntegerField(default=0)),
                ('dream_num', models.IntegerField(default=0)),
                ('sales_num', models.IntegerField(default=0)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
