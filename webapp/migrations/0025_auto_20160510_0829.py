# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0024_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='image',
            field=models.FileField(default=1, upload_to=b'customer/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='products',
            name='image',
            field=models.FileField(default=1, upload_to=b'products/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='users',
            name='image',
            field=models.FileField(default=1, upload_to=b'users/'),
            preserve_default=False,
        ),
    ]
