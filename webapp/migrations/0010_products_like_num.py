# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0009_auto_20160507_0521'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='like_num',
            field=models.IntegerField(default=0),
        ),
    ]
