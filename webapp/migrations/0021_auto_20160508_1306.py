# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0020_auto_20160508_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='busket_num',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='users',
            name='busket_price',
            field=models.IntegerField(default=0),
        ),
    ]
