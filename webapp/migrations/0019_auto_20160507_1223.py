# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0018_auto_20160507_1221'),
    ]

    operations = [
        migrations.RenameField(
            model_name='busket',
            old_name='product_id',
            new_name='product',
        ),
        migrations.RenameField(
            model_name='comments',
            old_name='product_id',
            new_name='product',
        ),
        migrations.RenameField(
            model_name='dream',
            old_name='product_id',
            new_name='product',
        ),
    ]
