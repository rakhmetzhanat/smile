# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0014_busket_comments_customer_dream_products_users'),
    ]

    operations = [
        migrations.AlterField(
            model_name='products',
            name='comments',
            field=models.ForeignKey(default=None, to='webapp.Comments'),
        ),
        migrations.AlterField(
            model_name='products',
            name='customer',
            field=models.ForeignKey(default=None, to='webapp.Customer'),
        ),
        migrations.AlterField(
            model_name='users',
            name='busket',
            field=models.ForeignKey(default=None, to='webapp.Busket'),
        ),
        migrations.AlterField(
            model_name='users',
            name='dream',
            field=models.ForeignKey(default=None, to='webapp.Dream'),
        ),
    ]
