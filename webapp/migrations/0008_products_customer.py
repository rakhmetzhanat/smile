# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0007_auto_20160505_1245'),
    ]

    operations = [
        migrations.AddField(
            model_name='products',
            name='customer',
            field=models.ForeignKey(default=1, to='webapp.Customer'),
            preserve_default=False,
        ),
    ]
