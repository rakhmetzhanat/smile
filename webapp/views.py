from webapp.models import Users, Products, Customer, Dream, Comments, Busket, Like, Friend, Message
from webapp.forms import UserForm, ProductForm
from django.shortcuts import render_to_response, render, redirect
from django.http import HttpResponse, Http404
from django.template import RequestContext, Context
from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token


def index_main(request):
	product_ls = Products.objects.all().order_by('date_created')
	
	return render_to_response('index_main.html', context_instance = RequestContext(request, {'products' : product_ls}));




def index_main_content(request):
	product_ls = Products.objects.filter(id__icontains  = request.GET['id'])
	comment_ls = Comments.objects.filter(product_id = request.GET['id']).order_by('-date_created')
	
	if 'id' in request.session:
		try:
		   	dream_ls = Dream.objects.get(user_id = request.session['id'], product_id = request.GET['id'])
		except Dream.DoesNotExist:
		    dream_ls = None
		try:
		    busket_ls = Busket.objects.get(user_id = request.session['id'], product_id = request.GET['id'])
		except Busket.DoesNotExist:
		    busket_ls = None
		try:
		    like_ls = Like.objects.get(user_id = request.session['id'], product_id = request.GET['id'])
		except Like.DoesNotExist:
		    like_ls = None
	else:	   
		dream_ls = None
		busket_ls = None
		like_ls = None

	return render_to_response('index_main_content.html', context_instance = RequestContext(request, {'comments' : comment_ls, 'products' : product_ls, 'dreams' : dream_ls, 'buskets' : busket_ls, 'likes' : like_ls}));
	



def index_customer_main(request):
	product_ls = Products.objects.filter(customer_id  = request.GET['id']).order_by('date_created')
	customer_ls = Customer.objects.filter(id = request.GET['id'])
	
	if 'id' in request.session:
		try:
		    friend_ls = Friend.objects.get(user_id = request.session['id'], customer_id = request.GET['id'])
		except Friend.DoesNotExist:
		    friend_ls = None
	else:
		friend_ls = None

	return render_to_response('index_customer_main.html', context_instance = RequestContext(request, {'products' : product_ls, 'customers' : customer_ls, 'friends' : friend_ls}));




def index_registration(request):
	form = UserForm()
	return render_to_response('index_registration.html',context_instance = RequestContext(request, {'form': form}));




def index_search(request):
	
	return render_to_response ('index_search.html',context_instance = RequestContext(request));




def index_profile(request):
	if 'id' in request.session:
		friend_ls = Friend.objects.filter(user_id = request.session['id'])
		message_ls = Message.objects.filter(user_id = request.session['id']).order_by('-date_created')
	else:
		friend_ls = None
		message_ls = None

	return render_to_response('index_profile.html',context_instance = RequestContext(request, {'messages_ls' : message_ls, 'friends' : friend_ls}));



	
def index_busket(request):
	if 'id' in request.session: 
		busket = Busket.objects.filter(user_id = request.session['id'])
		user = Users.objects.filter(id = request.session['id'])
	else:
		busket = None
		user = None	
	
	return render_to_response('index_basket.html', context_instance = RequestContext(request, {'products' : busket, 'users' : user}));




def index_dream(request):
	if 'id' in request.session:
		dream = Dream.objects.filter(user_id = request.session['id'])
	else:
		dream = None	
	
	return render_to_response('index_dream.html', context_instance = RequestContext(request, {'products' : dream}));




def index_store(request):
	if 'id' in request.session:
		friend_ls = Friend.objects.filter(user_id = request.session['id'])
	else:
		friend_ls = None	

	return render_to_response('index_store.html',context_instance = RequestContext(request, {'friends' : friend_ls}));




def index_about(request):

	return render_to_response('index_about.html',context_instance = RequestContext(request));




def logout(request):

	users_online = Session.objects.all().delete()
	
	return redirect('/');




def check_user(request):
	product_ls = Products.objects.all().order_by('date_created')
	
	try:
	    user = Users.objects.get(email=request.POST['email'], password=request.POST['password'])
	    request.session['name'] = user.name
	    request.session['surname'] = user.surname
	    request.session['email'] = user.email
	    request.session['telephone'] = user.telephone
	    request.session['password'] = user.password
	    request.session['user'] = user.user
	    request.session['id'] = user.id
	    request.session['image'] = user.image.url
	except Users.DoesNotExist:
	    user = None

	return render_to_response('index_main.html', context_instance = RequestContext(request, {'products' : product_ls}));




def insert_user(request):
	try:
	    user = Users.objects.get(email=request.POST['email'], password=request.POST['password'])
	except Users.DoesNotExist:
	    user = None

	if user == None:
		form = UserForm(request.POST, request.FILES)
		if form.is_valid():
			surname = request.POST['surname']
			name = request.POST['name']
			email = request.POST['email']
			telephone = request.POST['telephone']
			password = request.POST['password']
			image = request.FILES['image']
			register = Users.objects.create(image = image, surname = surname, name = name, email = email, telephone = telephone, password = password)
			register.save()
		else:
			return redirect('/registration/');
	else:
		return redirect('/registration/');

	return redirect('/');




def like(request):
	if 'id' in request.session:
		like = Like.objects.create(user_id = request.session['id'], product_id = request.GET['id'])

		product = Products.objects.get(id=request.GET['id'])
		register = Products.objects.filter(id__icontains  = request.GET['id']).update(like_num = (product.like_num + 1))		
		link = "../index_main_content/?id=" + request.GET['id']
	else:
		link = "../index_main_content/?id=" + request.GET['id']

	return redirect(link);	




def friend_add(request):
	friend = Friend.objects.create(user_id = request.session['id'], customer_id = request.GET['id'])
	link = "../index_customer_main/?id=" + request.GET['id']
	
	return redirect(link);




def friend_delete(request):
	friend = Friend.objects.get(user_id = request.session['id'], customer_id = request.GET['id'])
	friend.delete()

	return redirect("../store");




def message_user(request):
	message = Message.objects.create(user_id = request.session['id'], customer_id = request.POST['id'], content = request.POST['content'], sender = request.POST['from'])

	return redirect("../profile/");



def comment(request):
	comment = Comments.objects.create(user_id = request.session['id'], product_id = request.POST['id'], comment = request.POST['comment'])

	return redirect("../index_main_content/?id=" + request.POST['id']);



def message_customer(request):
	message = Message.objects.create(customer_id = request.session['id'], user_id = request.POST['id'], content = request.POST['content'], sender = request.POST['from'])

	return redirect("../customer_messages/");




def busket(request):
	product = Products.objects.get(id=request.GET['id'])
	users = Users.objects.get(id = request.session['id'])

	busket = Busket.objects.create(user_id = request.session['id'], product_id = request.GET['id'])
	user = Users.objects.filter(id = request.session['id']).update(busket_price = (users.busket_price + product.price), busket_num = (users.busket_num + 1))

	register = Products.objects.filter(id__icontains  = request.GET['id']).update(sales_num = (product.sales_num + 1))
	link = "../index_main_content/?id=" + request.GET['id']
	
	return redirect(link);	




def busket_delete(request):
	busket = Busket.objects.get(user_id = request.session['id'], product_id = request.GET['id'])
	busket.delete()

	product = Products.objects.get(id=request.GET['id'])
	users = Users.objects.get(id = request.session['id'])

	user = Users.objects.filter(id = request.session['id']).update(busket_price = (users.busket_price - product.price), busket_num = (users.busket_num - 1))
	register = Products.objects.filter(id__icontains  = request.GET['id']).update(sales_num = (product.sales_num - 1))
	
	link = "../basket"
	
	return redirect(link);	




def dream(request):
	dream = Dream.objects.create(user_id = request.session['id'], product_id = request.GET['id'])
	customer = Customer.objects.get(id=dream.product.customer_id)
	customers = Customer.objects.filter(id__icontains  = dream.product.customer_id).update(dream_sum = customer.dream_sum + 1)

	product = Products.objects.get(id=request.GET['id'])
	register = Products.objects.filter(id__icontains  = request.GET['id']).update(dream_num = (product.dream_num + 1))		
	link = "../index_main_content/?id=" + request.GET['id']
	
	return redirect(link);	




def dream_delete(request):
	dream = Dream.objects.get(user_id = request.session['id'], product_id = request.GET['id'])
	dream.delete()

	dream = Dream.objects.create(user_id = request.session['id'], product_id = request.GET['id'])
	customer = Customer.objects.get(id=dream.product.customer_id)
	customers = Customer.objects.filter(id__icontains  = dream.product.customer_id).update(dream_sum = customer.dream_sum - 1)

	product = Products.objects.get(id=request.GET['id'])
	register = Products.objects.filter(id__icontains  = request.GET['id']).update(dream_num = (product.dream_num - 1))	

	link = "../dream"
	
	return redirect(link);			






def customer_main(request):
	customer_id = request.session['id']
	product_ls = Products.objects.filter(customer_id  = customer_id).order_by('date_created')
	
	return render_to_response('customer_main.html', context_instance = RequestContext(request, {'products' : product_ls}));




def customer_profile(request):
	product_ls = Products.objects.all().order_by('date_created')
	
	return render_to_response('customer_profile.html', context_instance = RequestContext(request, {'products' : product_ls}));	




def customer_goods(request):
	product_ls = Products.objects.all().order_by('date_created')
	
	return render_to_response('customer_goods.html', context_instance = RequestContext(request, {'products' : product_ls}));	




def customer_messages(request):
	if 'id' in request.session:
		friend_ls = Friend.objects.filter(customer_id = request.session['id'])
	else:
		friend_ls = None	

	if 'id' in request.session:
		message_ls = Message.objects.filter(customer_id = request.session['id']).order_by('-date_created')
	else:
		message_ls = None	

	return render_to_response('customer_messages.html',context_instance = RequestContext(request, {'friends' : friend_ls, 'messages_ls' : message_ls}));	




def customer_statistics(request):
	product_ls = Products.objects.all().order_by('date_created')
	
	return render_to_response('customer_statistics.html', context_instance = RequestContext(request, {'products' : product_ls}));	




def customer_main_content(request):
	product_ls = Products.objects.filter(id = request.GET['id'])	
	comment_ls = Comments.objects.filter(product_id = request.GET['id']).order_by('-date_created')
	
	return render_to_response('customer_main_content.html', context_instance = RequestContext(request, {'products' : product_ls, 'comments' : comment_ls}));	




def customer_main_content_edit(request):
	product_ls = Products.objects.filter(id__icontains  = request.GET['id'])
	
	return render_to_response('customer_main_content_edit.html', context_instance = RequestContext(request, {'products' : product_ls}));	




def check_customer(request):	
	
	try:
	    customer = Customer.objects.get(email=request.POST['email'], password=request.POST['password'],)
	    request.session['name'] = customer.name
	    request.session['surname'] = customer.surname
	    request.session['email'] = customer.email
	    request.session['telephone'] = customer.telephone
	    request.session['password'] = customer.password
	    request.session['products_sum'] = customer.products_sum
	    request.session['city'] = customer.city
	    request.session['image'] = customer.image.url
	    request.session['id'] = customer.id
	except Customer.DoesNotExist:
	    customer = None
	  
	try:
	    product_ls = Products.objects.filter(customer_id  = request.session['id']).order_by('date_created')
	except Products.DoesNotExist:
	    product_ls = None

	return render_to_response('customer_main.html', context_instance = RequestContext(request, {'products' : product_ls}));




def insert_customer(request):

	try:
	    customer = Customer.objects.get(email=request.POST['email'], password=request.POST['password'],)
	except Customer.DoesNotExist:
	    customer = None

	if customer == None:
		form = UserForm(request.POST, request.FILES)
		if form.is_valid():
			surname = request.POST['surname']
			name = request.POST['name']
			email = request.POST['email']
			city = request.POST['city']
			telephone = request.POST['telephone']
			password = request.POST['password']
			image = request.FILES['image']
			register = Customer.objects.create(surname = surname, name = name, email = email, city = city, telephone = telephone, password = password, image = image)
			register.save()
		else:
			return redirect('/registration/');
	else:
		return redirect('/registration/');

	return redirect('/');




def insert_product(request):
	form = ProductForm(request.POST, request.FILES)
	if form.is_valid():
		title = request.POST['title']
		price = request.POST['price']
		content = request.POST['content']
		customer_id = request.session['id']	
		customer = Customer.objects.get(id=customer_id)

		image_1 = request.FILES['image_1']
		image_2 = request.FILES['image_2']
		image_3 = request.FILES['image_3']
		image_4 = request.FILES['image_4']

		customers = Customer.objects.filter(id__icontains  = customer_id).update(products_sum = (customer.products_sum + 1))
		product = Products.objects.create(title = title, price = price, content = content, customer_id = customer_id, image_1 = image_1, image_2 = image_2, image_3 = image_3, image_4 = image_4)
		product.save()
	else:
		return redirect('/customer_goods/');

	return redirect('/customer_main');




def update_product(request):
	form = ProductForm(request.POST, request.FILES)
	if form.is_valid():
		title = request.POST['title']
		price = request.POST['price']
		content = request.POST['content']
		customer_id = request.session['id']	
		customer = Customer.objects.get(id=customer_id)

		image_1 = request.FILES['image']
		image_2 = request.FILES['image_2']
		image_3 = request.FILES['image_3']
		image_4 = request.FILES['image_4']

		product = Products.objects.filter(id__icontains  = request.get['id']).create(title = title, price = price, content = content, customer_id = customer_id)
		product.save()
	else:
		return redirect('/registration/');

	return redirect('/customer_main');