from django.contrib import admin
from webapp.models import Users, Products, Customer, Dream, Comments, Busket, Like, Message, Friend 


admin.site.register(Users)
admin.site.register(Products)
admin.site.register(Customer)
admin.site.register(Dream)
admin.site.register(Comments)
admin.site.register(Busket)
admin.site.register(Like)
admin.site.register(Message)
admin.site.register(Friend)